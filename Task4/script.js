let canvas = document.getElementById('canvas');  //обращаемся к canvas
let ctx = canvas.getContext('2d');

// создаём необходимые переменные
let graph = [];                        // граф, с которым будет работать алгоритм 
ArrayCoord = [];                     // массив объектов, в котором будем хранить координаты каждоый точки для создания графа
pherAdd = [];                       // здесь храним надбавку ферамона для каждого маршрута после итерации
vertex = -1;                       // индекс для массивов (хранит в себе количесво точек, начиная с 0)
contol = true;                      //контроль функций


let rend = function (event) {             //функция рендеринга точки
   if (contol == true) {
      ctx.beginPath();
      ctx.arc(event.offsetX, event.offsetY, 15, 0, 2 * Math.PI, false);
      ctx.fillStyle = 'darkorange';
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = 'red';
      ctx.stroke();
   }
}
let newCoord = function (event) {    // функция заполнения массива объектов
   if (contol == true) {
      vertex++;                     //увеличивыем индекс при каждом создании точки
      ArrayCoord[vertex] = {          // заполняем массив объектов
         x: event.offsetX,
         y: event.offsetY
      }
   }
}
let fillGraph = function () {          //функция заполнения графа
   for (let i = 0; i <= vertex; i++) {
      graph[i] = [];
      for (let k = 0; k <= vertex; k++) {
         let x = 0;
         let y = 0;
         if (ArrayCoord[i].x >= ArrayCoord[k].x) {
            x = ArrayCoord[i].x - ArrayCoord[k].x;
         }
         if (ArrayCoord[k].x > ArrayCoord[i].x) {
            x = ArrayCoord[k].x - ArrayCoord[i].x;
         }
         if (ArrayCoord[i].y >= ArrayCoord[k].y) {
            y = ArrayCoord[i].y - ArrayCoord[k].y;
         }
         if (ArrayCoord[k].y > ArrayCoord[i].y) {
            y = ArrayCoord[k].y - ArrayCoord[i].y;
         }
         graph[i][k] = {
            dist: Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)),   //расстояние между вершинами
            pher: 0.6,                                         //начальное количество ферамонов
            wish: 0,                                           //желание муравья перейте в вершину
            allow: 0,                                          //надбавка ферамонов после завершения итерации
            visit: false                                        // доступ к вершине
         }
      }
   }
}
let wish = function (i) {           //вычисляем желания муравья перейти из i-ой веришны в остальные
   let sumWish = 0;
   for (let k = 0; k <= vertex; k++) {
      if (graph[i][k].visit == false && k != i) {
         graph[i][k].wish = Math.pow(graph[i][k].pher, 3) * Math.pow((200 / graph[i][k].dist), 4);  //желание перейти в k-ю вершину
         sumWish += graph[i][k].wish;    //желание перейти во все вершины                                             
      }

   }
   for (let k = 0; k <= vertex; k++) {
      if (graph[i][k].visit == false && k != i) {
         graph[i][k].wish /= sumWish;      //вероятность муравья перейти в k-ю вершину
      }
      else {
         graph[i][k].wish = 0;            //иначе она равна нулю
      }
   }

   for (let j = 0; j <= vertex; j++) {
      for (let k = 0; k <= vertex; k++) {
         if (k == i) {
            graph[j][k].visit = true;           //запрещаем переходить в вершины, в которых муравей уже бывал
         }
      }
   }
}

let roul = function (i, count) {          //рулетка выбора вершины для перехода
   let random = Math.random();         //генерируем число от 0 до 1
   let ver = [];
   let sum = 0;
   let J = 0;
   for (let k = 0; k <= count; k++) {                 //заполняем рулетку вероятностями для перехода в вершину
      for (let j = J; j <= vertex; j++) {
         if (graph[i][j].visit == false) {
            ver[k] = {
               ev: sum + graph[i][j].wish,
               ve: j
            }
            J = j + 1;
            break;
         }
      }
      sum += ver[k].ev;
      if (J > vertex) {
         break;
      }
   }
   if (ver.length == 1) {                       //возращаем выбранную вершину
      return ver[0].ve;
   }
   else {
      for (let k = 0; k < count; k++) {
         if (random > ver[k].ev && random <= ver[k + 1].ev) {
            return ver[k + 1].ve;
         }
         if (random >= 0 && random <= ver[0].ev) {
            return ver[0].ve;
         }
      }
   }
}
let addPher = function (sum, arrayCur) {         //вычисляем надбавку ферамона
   for (let k = 0; k < vertex; k++) {
      graph[arrayCur[k]][arrayCur[k + 1]].allow += 100 / sum;
   }

   for (let i = 0; i <= vertex; i++) {      //даём доступ к вершинам
      for (let k = 0; k <= vertex; k++) {
         graph[i][k].visit = false;
      }
   }
}
let newPher = function () {                     //добавляем ферамоны 
   for (let i = 0; i <= vertex; i++) {
      for (let k = 0; k <= vertex; k++) {
         graph[i][k].pher = (graph[i][k].pher * 0.95) + graph[i][k].allow;
         graph[i][k].allow = 0;   //обноляем надбавку
      }
   }
}






canvas.addEventListener('click', rend);      //запускам рендеринг по клику
canvas.addEventListener('click', newCoord);  // заполняем массив объектов 


document.getElementById('cl').addEventListener('click', function () {        //функция очистки canvas
   ctx.clearRect(0, 0, 720, 720);
   contol = true;      // разрешаем применять функции на canvas
   vertex = -1;
   graph = [];
   ArrayCoord = [];
   document.getElementById('dist').textContent = null;
});

document.getElementById('srt').addEventListener('click', function () {
   fillGraph();      //генерируем граф
   contol = false;     // запрещаем применять функции на canvas
   let count = vertex * 10;  // количество итераций
   if (vertex == -1) {
      alert("А точки расставить?!")
      contol = true;
   }
   if (vertex == 0) {
      alert("Ну давай хотя бы два!")
      contol = true;
   }
   if (vertex == 1) {
      ctx.beginPath();
      ctx.moveTo(ArrayCoord[0].x, ArrayCoord[0].y);
      ctx.lineTo(ArrayCoord[1].x, ArrayCoord[1].y);
      ctx.closePath();
      ctx.stroke();
      document.getElementById('dist').textContent = graph[0][1].dist;
   }
   else {
      while (count > 0) {
         if (count == 1) {       //если это последняя итерации, то муровей отрисовывает за собой путь
            let sumRoute = 0;    //расстояние, которое прошёл муровей
            let current = 0;     
            let cnt = vertex;
            ctx.beginPath();
            ctx.moveTo(ArrayCoord[current].x, ArrayCoord[current].y);
            while (cnt > 0) {
               wish(current);
               let p = current;
               current = roul(current, cnt - 1);
               sumRoute = sumRoute + graph[p][current].dist;  // вычисляем расстояние, которое прошёл муравей
               ctx.lineTo(ArrayCoord[current].x, ArrayCoord[current].y);
               cnt--;
            }
            ctx.closePath();
            ctx.stroke();
            count--;
            console.log(graph);
            document.getElementById('dist').textContent = sumRoute;
            console.log(sumRoute);
         }
         else {                  
            let sumRoute = 0;    //расстояние, которое прошёл муровей
            for (let k = 0; k <= vertex; k++) {       //количество муравьёв равно количеству  вершин
               let cnt = vertex;                      //задаём изначальное количество вершин, которое надо пройти муравья
               let current = k;                       //стартавая вершина
               let i = 0;                              //храним путь, который прошёл муровей
               let arrayCur = [];                     
               while (cnt > 0) {                   //обход муравья по вершинам
                  wish(current);                //вычисляем вероятность муравья перейти в каждую вершину
                  arrayCur[i] = current;        //обновляем путь
                  i++;
                  current = roul(current, cnt - 1);      //запускаем рулетку
                  sumRoute = sumRoute + graph[arrayCur[i-1]][current].dist;  // вычисляем расстояние, которое прошёл муравей
                  cnt--; // уменьшаем кол-во вершин
               }
               arrayCur[i] = current;     //замыкаем путь
               arrayCur[i + 1] = k;
               addPher(sumRoute, arrayCur); //вычисляем набдавку

            }
            newPher();    // как только все муравьи пройдут и итерации завершится увеличиваем даём добавки ферамонов 
            count--;
         }
      }
   }
});

