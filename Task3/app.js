VANTA.CLOUDS2({
   el: "#intro",
   mouseControls: true,
   touchControls: true,
   gyroControls: false,
   minHeight: 200.00,
   minWidth: 200.00,
   scale: 1.00,
   skyColor: 0x414186,
   speed: 2.00,
   texturePath: "./gallery/noise.png"
 })