let canvas = document.getElementById('canvas');  //обращаемся к canvas
let ctx = canvas.getContext('2d');

// создаём необхожимые нам переменные
let graph = [];                        // граф, с которым будет работать алгоритм 
ArrayCoord = [];                     // массив объектов, в котором будем хранить координаты каждоый точки для создания графа
vertex = -1;                       // индекс для массивов (хранит в себе количесво точек, начиная с 0)
contol = true;                      //контроль функций


let rend = function (event) {             //функция рендеринга точки
   if (contol == true) {   
      ctx.beginPath();
      ctx.arc(event.offsetX, event.offsetY, 5, 0, 2 * Math.PI, false);
      ctx.fillStyle = 'red';
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = 'red';
      ctx.stroke();
   }
}
let newCoord = function (event) {    // функция заполнения массива объектов
   if (contol == true) {
      vertex++;                     //увеличивыем индекс при каждом создании точки
      ArrayCoord[vertex] = {          // заполняем массив объектов
         x: event.offsetX,
         y: event.offsetY
      }
   }
}
let fillGraph = function () {          //функция заполнения графа
   for (let i = 0; i <= vertex; i++) {  //заполняем массив массивами 
      graph[i] = [];
   }
   for (let i = 0; i <= vertex; i++) {    
      for (let k = 0; k <= vertex; k++) {
         let x = 0;
         let y = 0;
         if (ArrayCoord[i].x >= ArrayCoord[k].x) {    
            x = ArrayCoord[i].x - ArrayCoord[k].x;
         }
         if (ArrayCoord[k].x > ArrayCoord[i].x) {
            x = ArrayCoord[k].x - ArrayCoord[i].x;
         }
         if (ArrayCoord[i].y >= ArrayCoord[k].y) {
            y = ArrayCoord[i].y - ArrayCoord[k].y;
         }
         if (ArrayCoord[k].y > ArrayCoord[i].y) {
            y = ArrayCoord[k].y - ArrayCoord[i].y;
         }
         graph[i][k] = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));   //расстояние между вершинами
      }
   }
}
canvas.addEventListener('click', rend);      //запускам рендеринг по клику
canvas.addEventListener('click', newCoord);  // заполняем массив объектов 


document.getElementById('cl').addEventListener('click', function () {        //функция очистки canvas
   ctx.clearRect(0, 0, 620, 620);
   contol=true;      // разрешаем применять функции на canvas
   vertex=-1;
  
});

document.getElementById('start').addEventListener('click', function () {
   fillGraph();      //генерируем граф
   contol=false;     // запрещаем применять функции на canvas

});
