let astar = {
   init: function(grid) {
     for(let x = 0; x < grid.length; x++) {
       for(let y = 0; y < grid[x].length; y++) {
         grid[x][y].f = 0;
         grid[x][y].g = 0;
         grid[x][y].h = 0;
         grid[x][y].debug = "";
         grid[x][y].parent = null;
       }  
     }
   },
   search: function(grid, start, end) {
     astar.init(grid);
  
     let openList   = [];
     let closedList = [];
     openList.push(start);
  
     while(openList.length > ) {
  
       // Возьмите наименьшее значение f (x) для обработки следующей
       let lowInd = ;
       for(let i=; i<openList.length; i++) {
         if(openList[i].f < openList[lowInd].f) { lowInd = i; }
       }
       let currentNode = openList[lowInd];
  
       // Конечный случай - результат был найден, вернуть пройденный путь
       if(currentNode.pos == end.pos) {
        let curr = currentNode;
        let ret = [];
         while(curr.parent) {
           ret.push(curr);
           curr = curr.parent;
         }
         return ret.reverse();
       }
  
       // Нормальный случай - переместить currentNode из открытого в закрытый, обработать каждого из его соседей
       openList.removeGraphNode(currentNode);
       closedList.push(currentNode);
       let neighbors = astar.neighbors(grid, currentNode);
  
       for(let i=; i<neighbors.length;i++) {
        let neighbor = neighbors[i];
         if(closedList.findGraphNode(neighbor) || neighbor.isWall()) {
           // Недопустимый узел для обработки, перейти к следующему соседу
           continue;
         }
  
         // g score - это кратчайшее расстояние от начала до текущего узла, нам нужно проверить
         // Путь, который мы достигли к этому соседу, самый короткий, который мы когда-либо видели
         let gScore = currentNode.g + 1; // 1 - это расстояние от узла до его соседа
         let gScoreIsBest = false;
  
  
         if(!openList.findGraphNode(neighbor)) {
           // Это первый раз, когда мы достигли этого узла, он должен быть лучшим
           // Кроме того, нам нужно взять h (эвристический) счет, поскольку мы еще этого не сделали
  
           gScoreIsBest = true;
           neighbor.h = astar.heuristic(neighbor.pos, end.pos);
           openList.push(neighbor);
         }
         else if(gScore < neighbor.g) {
           // Узел мы уже видели, но в прошлый раз у него был худший g (расстояние от старта)
           gScoreIsBest = true;
         }
  
         if(gScoreIsBest) {
           // Нашел оптимальный (пока что) путь к этому узлу. Храните информацию о том, как мы сюда попали и
           // насколько это хорошо на самом деле ...
           neighbor.parent = currentNode;
           neighbor.g = gScore;
           neighbor.f = neighbor.g + neighbor.h;
           neighbor.debug = "F: " + neighbor.f + "<br />G: " + neighbor.g + "<br />H: " + neighbor.h;
         }
       }
     }
  
     // Результат не найден - пустой массив означает, что путь не найден.
     return [];
   },
   heuristic: function(pos0, pos1) {
     // Это расстояние Манхэттена
     let d1 = Math.abs (pos1.x - pos0.x);
     let d2 = Math.abs (pos1.y - pos0.y);
     return d1 + d2;
   },
   neighbors: function(grid, node) {
     let ret = [];
     let x = node.pos.x;
     let y = node.pos.y;
  
     if(grid[x-1] && grid[x-1][y]) {
       ret.push(grid[x-1][y]);
     }
     if(grid[x+1] && grid[x+1][y]) {
       ret.push(grid[x+1][y]);
     }
     if(grid[x][y-1] && grid[x][y-1]) {
       ret.push(grid[x][y-1]);
     }
     if(grid[x][y+1] && grid[x][y+1]) {
       ret.push(grid[x][y+1]);
     }
     return ret;
   }
 };