let canvas = document.getElementById('canvas');
let ctx = canvas.getContext('2d');

// создаём необхожимые нам переменные
dots = [];                       // храним точки
vertex = -1;                       // индекс для массивов (хранит в себе количесво точек, начиная с 0)
contol = true;                      //контроль функций
groups = [];                        //храним кластеры
flag = false;                       

let rend = function (event) {             //функция рендеринга точки
   if (contol == true) {
      ctx.beginPath();
      ctx.arc(event.offsetX, event.offsetY, 20, 0, 2 * Math.PI, false);
      ctx.lineWidth = 1;
      ctx.strokeStyle = 'orange';
      ctx.stroke();
   }
}
let newCoord = function (event) {    // функция заполнения массива объектов
   if (contol == true) {
      vertex++;                     //увеличивыем индекс при каждом создании точки
      dots[vertex] = {          // заполняем массив объектов
         x: event.offsetX,
         y: event.offsetY,
         group: undefined        // изначельно точку никакой группе не принадлежит
      }
   }
}
function getRandomInRange(min, max) {                          //функция рандома
   return Math.floor(Math.random() * (max - min + 1)) + min;
}
let initGroups = function () {         // заполняем кластеры
   for (let i = 0; i < 3; i++) {       
      let color
      if (i == 0) {
         color = 'red';
      }
      if (i == 1) {
         color = 'blue';
      }
      if (i == 2) {
         color = 'green';
      }
      groups[i] = {
         id: 'group_' + i,          //номер кластера
         dots: [],                  //точки входящие в кластер
         color: color,              //цвет кластера
         center: {               //координаты центра
            x: getRandomInRange(20, 880),
            y: getRandomInRange(20, 780)
         },
      }
   }
}
let moveCenter = function () {         //центрируем кластер
   let finished = false;
   groups.forEach(function (group, i) {
      finished = true;
      if (group.dots.length == 0) return;

      let x = 0, y = 0;
      group.dots.forEach(function (dot) {
         x += dot.x;
         y += dot.y;
      });
      let oldPos = { x: group.center.x, y: group.center.y };
      group.center = {
         x: x / group.dots.length,
         y: y / group.dots.length
      };
      let newPos = { x: group.center.x, y: group.center.y };

      if (oldPos.x !== newPos.x || oldPos.y !== newPos.y) finished = false;

   });
}

let updateGroups = function () {                        //заполняем кластеры точками
   groups.forEach(function (g) { g.dots = []; });       
   dots.forEach(function (dot) {
      let min = Infinity;
      let group;
      groups.forEach(function (g) {
         let d = Math.pow(g.center.x - dot.x, 2) + Math.pow(g.center.y - dot.y, 2);
         if (d < min) {
            min = d;
            group = g;
         }
      });
      // update group
      group.dots.push(dot);
      dot.group = group;
   });
};

let step = function () {
   if (flag) {
      moveCenter();
   }
   else {
      updateGroups();
   }
   flag = !flag;
}
let rendCenter = function () {    // рендеринг кластеров
   for (let k = 0; k < 3; k++) {
      ctx.beginPath();
      ctx.arc(groups[k].center.x, groups[k].center.y, 5, 0, 2 * Math.PI, false);
      ctx.fillStyle = groups[k].color;
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = groups[k].color;
      ctx.stroke();
   }
   for (let i = 0; i <= vertex; i++) {
      ctx.beginPath();
      ctx.arc(dots[i].x, dots[i].y, 20, 0, 2 * Math.PI, false);
      ctx.fillStyle = dots[i].group.color;
      ctx.fill();
      ctx.lineWidth = 1;
      ctx.strokeStyle = dots[i].group.color;
      ctx.moveTo(dots[i].x, dots[i].y);
      ctx.lineTo(dots[i].group.center.x, dots[i].group.center.y)
      ctx.stroke();
   }
}


document.getElementById('cl').addEventListener('click', function () {        //функция очистки canvas
   ctx.clearRect(0, 0, 900, 700);
   contol = true;      // разрешаем применять функции на canvas
   vertex = -1;
   groups = [];
   dots = [];
});

canvas.addEventListener('click', rend);      //запускам рендеринг по клику
canvas.addEventListener('click', newCoord);  // заполняем массив объектов 

document.getElementById('str').addEventListener('click', function () {
   contol = false;     // запрещаем применять функции на canvas
   initGroups();
   let i = 6;
   while (i > 0) {
      step();
      i--;
   }
   rendCenter();
});