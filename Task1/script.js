let gCanvas = document.getElementById("gCanvas");
let gctx = gCanvas.getContext("2d");

function getRandomInRange(min, max) {                         //функция рандома
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

let gCanvasOffset;                                 //создаём переменные,с которыми будем работать
    CANVAS_WIDTH = gCanvas.width;
    CANVAS_HEIGHT = gCanvas.height;
    NODESIZE =20;   // Размер ячейки
    quan = document.getElementById('num'); //кол-во ячеек, введенное пользователем
    openSet = new Set();
    closedSet = new Set();
    gridPointsByPos = [];
    gridPoints = [];
    wallSet = new Set;

// используется для сохранения начальной и конечной точек во время сброса и т. д.
let startPoint;
let endPoint;

let path;
let mode = null;

// сообщает холсту, как рисовать узел
function nodeDrawer(context, target, lineW, strokeS, fillS) {
  context.beginPath();
  context.lineWidth = lineW;
  context.strokeStyle = strokeS;
  context.fillStyle = fillS;
  context.fillRect(target.posx, target.posy, target.size, target.size);
  context.rect(target.posx, target.posy, target.size, target.size);
  context.closePath();
  context.stroke();
}
// расстояние от узла до другого узла
function getDistance(nodeA, nodeB) {
  let distX = Math.abs(nodeA.posx - nodeB.posx);
  let distY = Math.abs(nodeA.posy - nodeB.posy);

  if (distX > distY) {
    return ((14 * distY) + (10 * (distX - distY)))

  }
  return (14 * distX + (10 * (distY - distX)));
}
class Node {

  constructor(id, size, posx, posy, walkable) {
    let F;

    let parent;
    this.inPath = false;
    this.getGCost = this.getValueG;
    this.getHCost = this.getValueH;

    this.size = size;
    this.posx = posx;
    this.posy = posy;
    this.walkable = walkable;

    this.id = id;

  }

  createStartNode() {
    nodeDrawer(gctx, this, 2, "black", "blue");

  }
  createEndNode() {
    nodeDrawer(gctx, this, 2, "black", "red");

  }
  toggleWalkable() {
    this.walkable = !this.walkable;
  }
  getValueH() {
    let endNodePosition = {
      posx: endPoint.x,
      posy: endPoint.y
    };
    return (getDistance(this, endNodePosition));

  }
  getValueG() {
    let startPointPosition = {
      posx: endPoint.x,
      posy: endPoint.y
    };
    return (getDistance(this, startPointPosition));
  }
  getValueF() {
    let fValue = (this.getValueH()) + (this.getValueG());
    return (fValue);
  }
  createWall() {
    nodeDrawer(gctx, this, 2, "black", "black");

  }
  drawOpenNode() {
    nodeDrawer(gctx, this, 2, "black", "green");

  }
  drawClosedNode() {
    nodeDrawer(gctx, this, 2, "black", "pink");
  }
  drawPath() {
    nodeDrawer(gctx, this, 2, "black", "purple");
  }
  drawNode() {

    gctx.beginPath();
    gctx.lineWidth = "2";
    gctx.strokeStyle = "black";
    gctx.fillStyle = "white";
    gctx.fillRect(this.posx, this.posy, this.size, this.size);
    gctx.rect(this.posx, this.posy, this.size, this.size);
    gctx.closePath();
    gctx.stroke();
    if (this.inPath === true) {
      this.drawPath();
    }
    if (this.walkable === false) {
      this.createWall();
      return;
    }
    if (this.posx == startPoint.x && this.posy == startPoint.y) {
      this.createStartNode();
      return;
    }
    if (this.posx == endPoint.x && this.posy == endPoint.y) {
      this.createEndNode();
    }

  }
}
class Grid {
  constructor(width, height, posx, posy, gridPoints) {
    this.width = width;
    this.height = height;
    this.posx = posx;
    this.posy = posy;
    this.gridPoints = gridPoints;
  }
  createGrid() {
    let tempNode;
    let countNodes = 0;
    gctx.beginPath();
    gctx.lineWidth = "1";
    gctx.strokeStyle = "black";
    gctx.rect(0, 0, this.width, this.height);
    gctx.stroke();

    for (let i = 0; i < this.width; i += NODESIZE) {
      gridPointsByPos[i] = [];

      for (let j = 0; j < this.height; j += NODESIZE) {
        gridPointsByPos[i][j] = countNodes;
        tempNode = new Node(countNodes, NODESIZE, i, j, true);
        if (wallSet.has(countNodes)) {
          tempNode.walkable = false;
        }
        tempNode.drawNode();
        tempNode.F = tempNode.getValueF();
        gridPoints.push(tempNode);
        countNodes++;
      }
    }

  }
}

// любая точка в 2D пространстве
class Vec2 {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}
gCanvasOffset = new Vec2(gCanvas.offsetLeft, gCanvas.offsetTop);
startPoint = new Vec2(0, 0);
endPoint = new Vec2(0, 0);


class PathFindingAlg {
  constructor(grid, startNode, endNode) {
    this.grid = grid;
    this.startNode = gridPointsByPos[startNode.x][startNode.y];
    this.endNode = gridPointsByPos[endNode.x][endNode.y];
    this.currentNode = null;

    this.openSet = [];
    this.closedset = [];
  }
  findPath() {
    openSet.clear();
    closedSet.clear();

    let grid = this.grid; // сетка, с которой мы работаем

    let currentNode = this.startNode;  // currentNode, по умолчанию сейчас начальный узел

    let endNode = gridPoints[this.endNode];   // целевой узел
    let startNode = gridPoints[this.startNode];

    let tempArray;

    let newMovementCost; // новая стоимость движения к соседу

    openSet.add(gridPoints[currentNode]);
    while (openSet.size > 0) {
      tempArray = Array.from(openSet);

      currentNode = tempArray[0];

      for (var i = 1; i < tempArray.length; i++) {
  
        if (tempArray[i].getValueF() < currentNode.getValueF() || tempArray[i].getValueF() == currentNode.getValueF() && tempArray[i].getValueH() < currentNode.getValueH()) {
          currentNode = tempArray[i]; // устанавливает для currentNode значение openSetI, если оно имеет меньшее значение F, или значение = F с меньшим значением HCost.

        }
      }

      // выход из цикла либо с наименьшим значением F, либо с комбинированным значением H и значением F

      openSet.delete(currentNode);

      currentNode.drawClosedNode();

      closedSet.add(currentNode);


      // может потребоваться поставить это после getNighbors .... затем заменить closedSet.hasIn (neighbourNode на currentNode
      if (currentNode.id == startNode.id) {
        currentNode.drawNode();
      }
      if (currentNode.id == endNode.id) {
        currentNode.drawNode();
      }
      if (currentNode.walkable == false) {
        currentNode.drawNode();
      }

      if (currentNode.id == endNode.id) {
        retracePath(startNode, endNode);
        // достигнув последней точки, выходим из цикла.

        return; // выход из цикла
      }
      getNeighbors(currentNode).forEach(function (neighbor) {

        let neighborNode = gridPoints[neighbor];
        let neighborH = neighborNode.getHCost();
        let neighborG = neighborNode.getGCost();

        let currentG = currentNode.getGCost();
        let currentH = currentNode.getHCost();

        if (!neighborNode.walkable || closedSet.has(neighborNode)) {

          return; // действует как продолжение, нет необходимости продолжать, если стена уже проверена.

        }

        newMovementCost = currentG + (getDistance(currentNode, neighborNode));

        if (newMovementCost < neighborG || !openSet.has(neighborNode)) {

          neighborNode.gCost = newMovementCost;
          neighborNode.hCost = neighborH;
          neighborNode.parent = currentNode;

          if (!openSet.has(neighborNode)) {
            // помещаем соседний узел в openSet, чтобы проверить его на соответствие другим открытым значениям
            openSet.add(neighborNode);

            neighborNode.drawOpenNode();

          }
        }

      })
    }

  }

}

// левый верхний угол сетки будет расположен в точке 0,0, чтобы заполнить холст
let grid = new Grid(CANVAS_WIDTH, CANVAS_HEIGHT, 0, 0);
grid.createGrid();

let myPath = new PathFindingAlg(grid, startPoint, endPoint);

function retracePath(startNode, endNode) {
  path = new Set();
  let currentNode = endNode;
  let reverseArray;
  while (currentNode != startNode) {
    path.add(currentNode);
    currentNode = currentNode.parent;
    currentNode.inPath = true;
    if (currentNode != startNode)
      currentNode.drawPath();
  }
  reverseArray = Array.from(path);
  reverseArray.reverse();
  path = new Set(reverseArray);

}
// список соседей
function getNeighbors(node) {
  let checkX;
  let checkY;
  let neighborList = [];
  let tempList = [];
  for (let x = -NODESIZE; x <= NODESIZE; x += NODESIZE) {
    for (let y = -NODESIZE; y <= NODESIZE; y += NODESIZE) {
      if (x == 0 && y == 0) {
        continue;
      }
      checkX = node.posx + x;
      checkY = node.posy + y;

      if (checkX >= 0 && checkX <= CANVAS_WIDTH - NODESIZE && checkY >= 0 && checkY <= CANVAS_HEIGHT - NODESIZE) {

        tempList.push(gridPointsByPos[checkX][checkY]);
      }
    }
  }
  neighborList = tempList;

  return (neighborList);

}
// UI, кнопки и события / функции нажатия
function random() {
  resetWalls();
  NODESIZE = 620 / getRandomInRange(2, 30);
  gCanvasOffset = new Vec2(gCanvas.offsetLeft, gCanvas.offsetTop);
  startPoint = new Vec2(0, 0);
  endPoint = new Vec2(0, 0);
}
// очищаем путь БЕЗ очистки стен
function reset() {
  gridPoints = [];  // сбрасывает gridPoints, чтобы очистить стены и т. д. при сбросе.
  gridPointsByPos = [];
  openSet.clear();
  closedSet.clear();
  gctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
  grid.createGrid();

}
// сбрасывает все, ВКЛЮЧАЯ стены
function resetWalls() {

  wallSet.clear();
  reset();
}

//создает функции кнопок
document.getElementById("gen").onclick = function () {
  reset();
  NODESIZE = 620 / quan.value;
  gCanvasOffset = new Vec2(gCanvas.offsetLeft, gCanvas.offsetTop);
  startPoint = new Vec2(0, 0);
  endPoint = new Vec2(0, 0);

}
document.getElementById("btnStartPoint").addEventListener("click", function (event) {
  mode = "startPoint";
});
document.getElementById("btnEndPoint").addEventListener("click", function (event) {
  mode = "endPoint";
});
document.getElementById("btnWall").addEventListener("click", function (event) {
  mode = "wall";
});
document.getElementById("wallReset").addEventListener("click", function (event) {
  resetWalls();
})
document.getElementById('cl').addEventListener('click', function (event) {
  reset();
});
document.getElementById("btnBeginPathFind").addEventListener("click", function (event) {
  reset();
  myPath = new PathFindingAlg(grid, startPoint, endPoint);
  myPath.findPath();
});
document.getElementById('rd').addEventListener('click', random)
//сообщает холсту, что делать при нажатии
gCanvas.addEventListener('click', function (event) {
  let x = event.pageX - $(gCanvas).position().left;
  let y = event.pageY - $(gCanvas).position().top;

  gridPoints.forEach(function (element) {
    if (y > element.posy && y < element.posy + element.size && x > element.posx && x < element.posx + element.size) {

      if (mode === "startPoint") {

        startPoint = new Vec2(element.posx, element.posy);
        reset();
      } else if (mode === "wall") {
        wallSet.add(element.id);
        element.toggleWalkable();
        element.drawNode();

      } else if (mode === "endPoint") {
        endPoint = new Vec2((element.posx), (element.posy));
        reset();
      } else {
        alert("Select an action!")
      }

    }
  });
}, false);